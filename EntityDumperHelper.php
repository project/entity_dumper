<?php

namespace Drush\Commands\entity_dumper;

use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Plugin\DataType\EntityReference;
use Drupal\Core\TypedData\Type\StringInterface;
use Drupal\field\Entity\FieldConfig;

class EntityDumperHelper {

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param array $descend
   * @param $maxdepth
   */
  public static function dumpEntity(ContentEntityInterface $entity, array $descend, $maxdepth): string {
    $descend = array_flip($descend);
    $maxdepth = (int) $maxdepth;
    $seen = new \SplObjectStorage();
    $return = [];
    foreach ($entity->getTranslationLanguages() as $language) {
      $langcode = $language->getId();
      self::descend($entity->getTranslation($langcode), $descend, $maxdepth, "$langcode:", $seen, $return);
    }
    return implode("\n", $return);
  }

  public static function descend(ContentEntityInterface $entity, array $descend, int $maxDepth, string $indent, \SplObjectStorage $seen, &$return): void {
    $langcode = $entity->language()->getId();
    if (!isset($seen[$entity])) {
      $seen[$entity] = [];
    }
    $seen[$entity] = $seen[$entity] + [$langcode => TRUE];
    if ($maxDepth && strlen($indent) >= $maxDepth * 2) {
      $return[] = "$indent **max depth reached**";
      return;
    }
    foreach ($entity->getFieldDefinitions() as $fieldName => $definition) {
      if ($definition->getFieldStorageDefinition()->getType() === 'metatag') {
        // @TODO look into metatag_generate_entity_metatags() for dumping.
        // Otherwise, metatag is not compatible with this module.
        continue;
      }
      if ($definition instanceof FieldConfig || ($definition instanceof ThirdPartySettingsInterface && $definition->getThirdPartySetting('entity_dumper', 'include'))) {
        /** @var \Drupal\Core\Field\FieldItemInterface $fieldItem */
        $fieldItemList = $entity->get($fieldName);
        $multiple = $definition->getFieldStorageDefinition()
            ->getCardinality() != 1;
        foreach ($fieldItemList as $delta => $fieldItem) {
          $properties = $fieldItem->getProperties(TRUE);
          foreach ($properties as $propertyName => $property) {
            $prefix = "$indent$fieldName";
            if ($multiple) {
              $prefix .= ":$delta";
            }
            try {
              $propertyValue = @$property->getValue();
            }
            catch (\Throwable $e) {
              // Metatag -> token blows up when trying to make node:uri for
              // new nodes.
              continue;
            }
            if ($property instanceof EntityReference && ($child = $propertyValue) && $child instanceof ContentEntityInterface) {
              $ids = ['i:' . $child->id()];
              if ($r = $child->getRevisionId()) {
                $ids[] = "r:$r";
              }
              $ids[] = "b:" . $child->bundle();
              if ($child->hasTranslation($langcode)) {
                $child = $child->getTranslation($langcode);
              }
              $alreadySeen = isset($seen[$child][$child->language()->getId()]) ? ' ** already seen **' : '';
              $return[] = sprintf("%s => %s %s%s", $prefix, $child->getEntityTypeId(), implode(' ', $ids), $alreadySeen);
              if (!$alreadySeen && isset($descend[$child->getEntityTypeId()])) {
                static::descend($child, $descend, $maxDepth, $indent . '  ', $seen, $return);
              }
            }
            if ($property instanceof StringInterface) {
              if (count($properties) > 1) {
                $prefix .= ":$propertyName";
              }
              $string = $propertyValue ? explode("\n", $propertyValue, 2)[0] : '';
              $length = 116 - strlen($prefix);
              if (strlen($string) > $length) {
                $delimiter = '^';
                $string = substr($propertyValue, 0, $length);
              }
              else {
                $delimiter = '=';
              }
              $return[] = sprintf("%s %s %s", $prefix, $delimiter, $string);
            }
          }
        }
      }
    }
  }

}
