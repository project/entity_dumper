<?php

namespace Drush\Commands\entity_dumper;

use Drush\Attributes as CLI;
use Drush\Boot\DrupalBootLevels;
use Drush\Commands\DrushCommands;

/**
 * Entity dumper commandfile.
 */
class EntityDumperCommands extends DrushCommands {

  #[CLI\Command(name: 'entity-dumper')]
  #[CLI\Bootstrap(level: DrupalBootLevels::FULL)]
  #[CLI\Argument(name: 'entity_type_id', description: 'The entity type id')]
  #[CLI\Argument(name: 'entity_id', description: 'The entity id')]
  #[CLI\Option(name: 'descent', description: 'A comma separated list of entity type IDs to descend into. Defaults to paragraph.')]
  #[CLI\Option(name: 'max-depth', description: 'Maximum descend depth. Avoids infinite loops. Defaults to 0 which means no limit.')]
  #[CLI\Usage(name: 'entity-dumper --descend=paragraph,media node 92920', description: 'Create an entity dump of node 92920 descending into paragraphs and media.')]
  public function dump(string $entity_type_id, $entity_id, $options = ['descend' => 'paragraph', 'max-depth' => 0]) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    $descend = explode(',', $options['descend']);
    include_once __DIR__ . '/EntityDumperHelper.php';
    $this->output()->write(EntityDumperHelper::dumpEntity($entity, $descend, $options['max-depth']));
  }

}
